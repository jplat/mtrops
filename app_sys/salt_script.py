#!/usr/bin/python2.7
#coding:utf-8


#salt api
def Client(tgt, module, argv,runas):
    '''salt api'''
    import salt.client
    local = salt.client.LocalClient()
    if argv:
        result = local.cmd(tgt, module, argv, tgt_type='list',kwarg={'runas': runas})
    else:
        result = local.cmd(tgt, module, tgt_type='list')
    return result


#执行函数
def script(tgt,script,runas):
    '''执行脚本'''

    module = 'cmd.script'
    argv = [script]
    result = Client(tgt, module, argv,runas)
    return result



def cmdrun(tgt,cmd,runas):
    '''执行命令'''
    module = 'cmd.run'
    argv = [cmd]
    result = Client(tgt, module, argv,runas)
    return result



def upload_file(tgt,src,dest,runas):
    """上传文件"""
    module = 'cp.get_file'
    argv = [src,dest,"makedirs=True"]
    result = Client(tgt, module, argv,runas)
    return result

