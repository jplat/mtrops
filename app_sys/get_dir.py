#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def Client(tgt, module, argv,runas):

    import salt.client

    local = salt.client.LocalClient()
    if argv:
        result = local.cmd(tgt, module, [argv], tgt_type='list', kwarg={'runas': runas})
    else:
        result = local.cmd(tgt, module, tgt_type='list')
    return result


def main(ip,dir,runas):
    cmd = "cd %s && pwd && ls -al| grep -v total | awk '{print $1,$9}'" % dir
    module = "cmd.run"
    tgt = [ip]
    result = Client(tgt,module,cmd,runas)
    return result


if __name__ == "__main__":
    ip='192.168.1.218'
    dir='/root/'
    runas = 'root'
    print main(ip,dir,runas)