
from django.conf.urls import url,include
from app_sys import views


urlpatterns = [
    url('^depend/$', views.Depend),
    url('^editdepend/$', views.EditDepend),
    url('^deldepend/$', views.DelDepend),
    url('^addepend/$', views.AddDepend),
    url('^install/$', views.Install),

    url('^file/$',views.FileMG),
    url('^file/?ip=(\d+.\d+.\d+.\d+)/?dir=(\S+)/$',views.FileMG),

    url('^chdir/$',views.ch_dir),
    url('^upfile/$', views.Upfile),
    url('^downfile/$', views.Downfile),
    url('^removefile/$',views.Removefile),

    url('^batch/$',views.batch),
    url('^cmdrun/$',views.batch_cmd_run),
    url('^uploadfile/$',views.batch_upload_file),
    url('^script/$',views.batch_script),

    url('^cron/$',views.CronView.as_view()),


]

