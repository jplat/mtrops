
from django.conf.urls import url,include
from app_log import views


urlpatterns = [
    url('^audit/$', views.audit),
    url('^auditlog/$',views.audit_check),
    url('^userlog/$',views.user_log),
]

