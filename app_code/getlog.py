#coding:utf-8

import commands
import time
import json

def get_log(ip, site_path, site_name,file_path):

    cmdform = '-5  --pretty=format:"%H-%an-%ad-%s"'

    cmd_version = "salt '%s' cmd.run 'cd %s/%s && git log %s %s'" % (ip, site_path, site_name, cmdform,file_path)

    logmsg = commands.getoutput(cmd_version)

    log_list = []

    if file_path=="":
        file_path="ALL"

    for i in logmsg.split("\n")[1:]:
        log_info = i.strip().split('-')

        current_version = log_info[0]

        version_info = log_info[3]

        author = log_info[1]

        upcode_date = log_info[2]

        str_date = upcode_date.strip('+0800').strip()

        array_date = time.strptime(str_date, "%a %b %d %H:%M:%S %Y")

        upcode_date = time.strftime('%Y-%m-%d %H:%M:%S', array_date)


        log_list.append({"ip":ip,"site_name":site_name,"file_path":file_path,"version_id":current_version,"author":author,"version_info":version_info,"update_time":upcode_date})

    return log_list



if __name__ == "__main__":
    ip="192.168.1.218"
    site_path="/opt"
    site_name="mtrops"
    file_path = "test.py"
    print get_log(ip, site_path, site_name, file_path)
